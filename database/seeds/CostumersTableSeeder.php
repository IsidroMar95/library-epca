<?php

use Illuminate\Database\Seeder;

class CostumersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Costumer::class,20)->create();
    }
}
