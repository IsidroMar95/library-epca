<?php

use Illuminate\Database\Seeder;

class BorrowingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Borrowing::class, 30)->create()->each(function (App\Borrowing $borrowing) {
            $borrowing->copies()->attach([
                rand(1, 2),
                rand(2, 4)
            ]);
        });

    }
}
