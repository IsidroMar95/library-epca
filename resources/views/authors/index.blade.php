@extends('layout') 
@section('content')

<h2 class="mb-4 mt-4">Autores <i class="fas fa-pen-fancy"></i> </h2>


<a class="btn btn-success mt-2 mb-3" href="{{ route('autores.create') }}" role="button">Añadir nuevo autor <i class="fas fa-plus"></i></a>


<div class="card mb-4">
    <div class="card-body">
        <table id="table-records" class="table table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th>Autor</th>
                    <th class="actions">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($authors as $author)
                <tr>
                    <td>{{ $author->id}}</td>
                    <td>{{ $author->name}}</td>
                    <td>
                        <a href="{{ route('autores.edit',$author->id)}}" class="btn btn-icon btn-warning" data-toggle="tooltip" title="Editar"><i class="fa fa-fw fa-edit"></i></a>
                        <form class="d-inline" action="{{ route('autores.destroy', $author->id) }}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-fw fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    
@stop 
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#table-records').dataTable( {
                "language": {
                "url": "{{ asset('js/Spanish.json') }}"
                 },
                 "ordering": false,
                 "pageLength": 5
            } );
        });
    </script>
@endsection