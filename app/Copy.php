<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Copy extends Model
{
    public $timestamps = false;

    protected $fillable = ['book_id', 'code', 'status', 'details'];

    public function setNameAttribute($code)
    {
        $this->attributes['code'] = $code;
        $this->attributes['slug'] = str_slug($code);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }


    public function borrowings()
    {
        return $this->belongsToMany(Borrowing::class);
    }


}
