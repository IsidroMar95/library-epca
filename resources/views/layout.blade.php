<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="{{URL::to('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('css/fontawesome.all.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('css/bootadmin.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('css/select2.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('css/select2-bootstrap4.min.css')}}">
    <link rel="shortcut icon" href="{{URL::to('img/favicon.ico') }}" type="image/x-icon">

    <title>{{config('app.name') }} | Dashboard</title>
</head>

<body class="bg-light">

    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <a class="sidebar-toggle mr-3" href="#"><i class="fa fa-bars"></i></a>
        <a class="navbar-brand" href="{{route('dashboard') }}">Administración {{config('app.name') }}</a> {{--
        <div class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-envelope"></i> 5</a></li>
                <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-bell"></i> 3</a></li>
                <li class="nav-item dropdown">
                    <a href="#" id="dd_user" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Doe</a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd_user">
                        <a href="#" class="dropdown-item">Profile</a>
                        <a href="#" class="dropdown-item">Logout</a>
                    </div>
                </li>
            </ul>
        </div> --}}
    </nav>

    <div class="d-flex">
    @include('partials.sidenav')

        <div class="content p-4">
            @if(session('info'))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{session('info')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif  
        
        
            @if(session('error'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                {{session('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif


            @if(count($errors))  
            <div class="alert alert-danger alert-dismissible" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                         @endforeach
                    </ul>
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                </button>
            </div>          
            @endif
            @yield('content')
        </div>
    </div>

    <script src="{{URL::to('js/jquery.min.js') }}"></script>
    <script src="{{URL::to('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{URL::to('js/datatables.min.js') }}"></script>
    <script src="{{URL::to('js/moment.min.js') }}"></script>
    <script src="{{URL::to('js/bootadmin.min.js') }}"></script>
    <script src="{{URL::to('js/select2.min.js') }}"></script>
    @yield('scripts')

</body>

</html>