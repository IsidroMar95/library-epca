<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costumer extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'first_surname', 'second_surname', 'phone', 'email'];

    public function borrowing()
    {
        return $this->hasMany(Borrowing::class);
    }
}
