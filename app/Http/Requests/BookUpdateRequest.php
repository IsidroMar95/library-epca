<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|unique:books,title,' . $this->book,
            'author_id'     => 'required|integer',
            'category_id'   => 'required|integer',
            'editorial_id'  => 'required|integer',
            'isbn'          => 'required|max:13|unique:books,isbn,' . $this->book,
        ];
    }
}
