<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costumer;
use App\Http\Requests\CostumerStoreRequest;
use App\Http\Requests\CostumerUpdateRequest;

class CostumerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costumers = Costumer::orderBy('id', 'DESC')->get();

        return view('costumers.index', compact('costumers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('costumers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostumerStoreRequest $request)
    {
        //Validar
        $costumer = Costumer::create($request->all());
        return redirect()->route('clientes.index')
            ->with('info', 'Cliente creado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $costumer = Costumer::find($id);
        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $costumer = Costumer::find($id);
        return view('costumers.edit', compact('costumer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CostumerUpdateRequest $request, $id)
    {
        //Validar
        $costumer = Costumer::find($id);

        $costumer->fill($request->all())->save();

        return redirect()->route('clientes.edit', $costumer->id)
            ->with('info', 'Cliente actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $costumer = Costumer::find($id);
        $costumer->delete();

        return redirect()->route('clientes.index')
            ->with('info', 'Cliente eliminado exitosamente.');
    }
}
