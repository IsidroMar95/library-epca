<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CopyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book_id'   => 'required|integer',
            'code'      => 'required|max:20|unique:copies,code,'. $this->copy,
            'status'    => 'required|in:DISPONIBLE,PRESTADO',
            'details'   => 'max:200',
        ];
    }
}
