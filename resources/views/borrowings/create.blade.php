@extends('layout') 
@section('content')
<h2 class="mb-4 mt-4">Realizar prestamos <i class="fas fa-handshake"></i> </h2>

<div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
        Campos con * son obligatorios.
    </div>
    <div class="card-body">         
            @include('borrowings.partials.form')
    </div>
</div>
<div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
        Ejemplares añadidos.
    </div>
    <div class="card-body">
        @if(session()->has('copies'))
        <table id="table-records" class="table table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th>Editorial</th>
                    <th class="actions">Acciones</th>
                </tr>
            </thead>
            <tbody>
                    {{-- @foreach(Session::get('copy') as $copy)
                    {{$copy->id}}
                    @endforeach --}}

                @foreach (Session::get('copies') as $copies)
                <tr>
                    <td> '</td>
                </tr>

                @endforeach


            </tbody>
        </table>
    @else 
    echo 'No hay datos en la sesión';
    @endif
    </div>
</div>

















@stop
