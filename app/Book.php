<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    public $timestamps = false;

    protected $fillable = ['title', 'author_id', 'category_id', 'editorial_id', 'isbn'];



    public function setNameAttribute($title)
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = str_slug($title);
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function editorial()
    {
        return $this->belongsTo(Editorial::class);
    }

    public function copies()
    {
        return $this->hasMany(Copy::class);
    }

}
