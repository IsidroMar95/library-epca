@extends('layout') 
@section('content')
<h2 class="mb-4 mt-4">Editar Autor <i class="fas fa-pen-fancy"></i> </h2>

<div class="card mb-4">
    <div class="card-header bg-white font-weight-bold">
        Campos con * son obligatorios.
    </div>
    <div class="card-body">
        {!! Form::model($costumer, ['route' => ['clientes.update',$costumer->id], 'method' => 'PUT','autocomplete' => 'off']) !!}
                        
            @include('costumers.partials.form')

        {!! Form::close() !!}
    </div>
</div>

@stop
