@extends('layout') 
@section('content')

<h2 class="mb-4 mt-4">Dashboard <i class="fas fa-tachometer-alt "></i> </h2>

<div class="row mb-4">
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-2">
        <div class="d-flex border">
            <div class="bg-primary text-light p-4">
                <div class="d-flex align-items-center h-100">
                    <i class="fas fa-book fa-3x"></i>
                </div>
            </div>
            <div class="flex-grow-1 bg-white p-4">
                <p class="text-uppercase text-secondary mb-0">Libros</p>
                <h3 class="font-weight-bold mb-0">{{$books->count()}}</h3>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-2">
        <div class="d-flex border">
            <div class="bg-success text-light p-4">
                <div class="d-flex align-items-center h-100">
                    <i class="fas fa-glasses fa-3x"></i>
                </div>
            </div>
            <div class="flex-grow-1 bg-white p-4">
                <p class="text-uppercase text-secondary mb-0">Clientes</p>
                <h3 class="font-weight-bold mb-0">{{$costumers->count() }}</h3>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-2">
        <div class="d-flex border">
            <div class="bg-danger text-light p-4">
                <div class="d-flex align-items-center h-100">
                    <i class="fas fa-book-reader fa-3x"></i>
                </div>
            </div>
            <div class="flex-grow-1 bg-white p-4">
                <p class="text-uppercase text-secondary mb-0">Prestamos</p>
                <h3 class="font-weight-bold mb-0">{{$borrowings->count() }}</h3>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-2">
        <div class="d-flex border">
            <div class="bg-info text-light p-4">
                <div class="d-flex align-items-center h-100">
                    <i class="fas fa-book-open fa-3x"></i>
                </div>
            </div>
            <div class="flex-grow-1 bg-white p-4">
                <p class="text-uppercase text-secondary mb-0">Ejemplares</p>
                <h3 class="font-weight-bold mb-0">1{{$copies->count() }}</h3>
            </div>
        </div>
    </div>
</div>


<div class="card">
    <div class="card-header bg-white font-weight-bold">
       Últimos libros agregados recientemente
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th width="20%">Titulo</th>
                    <th>Autor</th>
                    <th>Categoria</th>
                    <th>Ejemplares</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($books as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->title }}</td>
                    <td>{{ $book->isbn }}</td>
                    <td>{{ $book->author->name }}</td>
                    <td>{{ $book->editorial->name }}</td>
                    <td>{{ $book->category->name }}</td>
                    <td>{{ $book->copies->count() }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="card mt-5">
    <div class="card-header bg-white font-weight-bold">
        Últimos clientes agregados recientemente
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Email</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="#">00000077</a></td>
                    <td>Allene</td>
                    <td>248-813-9063</td>
                    <td>Sonya63@gmail.com</td>
                </tr>
                <tr>
                    <td><a href="#">00000078</a></td>
                    <td>Manuela</td>
                    <td>545-922-2470</td>
                    <td>Korey_McLaughlin34@hotmail.com</td>
                </tr>
                <tr>
                    <td><a href="#">00000079</a></td>
                    <td>Kelvin</td>
                    <td>178-922-3778</td>
                    <td>Tate76@hotmail.com</td>
                </tr>
                <tr>
                    <td><a href="#">00000080</a></td>
                    <td>Brandts</td>
                    <td>903-564-5322</td>
                    <td>Lulu40@yahoo.com</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
{{--
<div class="card mt-5">
    <div class="card-header bg-white font-weight-bold">
        Estado de Ejemplares
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Libro</th>
                    <th scope="col">Código</th>
                    <th scope="col">Estatus</th>
                    <th scope="col">Detalles</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="#">repudiandae</a></td>
                    <td>91380</td>
                    <td><span class="badge badge-success">DISPONIBLE</span></td>
                    <td>voluptas incidunt vitae</td>

                </tr>
                <tr>
                    <td><a href="#">est</a></td>
                    <td>L63880</td>
                    <td><span class="badge badge-danger">PRESTADO</span></td>
                    <td>voluptas fugiat veritatis</td>

                </tr>
                <tr>
                    <td><a href="#">in</a></td>
                    <td>17112</td>
                    <td><span class="badge badge-danger">PRESTADO</span></td>
                    <td>reiciendis ut beatae</td>

                </tr>
                <tr>
                    <td><a href="#">recusandae</a></td>
                    <td>Do65898</td>
                    <td><span class="badge badge-success">DISPONIBLE</span></td>
                    <td>facere possimus est</td>

                </tr>
            </tbody>
        </table>
    </div>
</div> --}} 
@stop