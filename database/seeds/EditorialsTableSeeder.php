<?php

use Illuminate\Database\Seeder;

class EditorialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Editorial::class,3)->create();
    }
}
