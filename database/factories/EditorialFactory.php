<?php

use Faker\Generator as Faker;

$factory->define(App\Editorial::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->unique()->word(5),
    ];
});
