<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Editorial;
use App\Http\Requests\EditorialStoreRequest;
use App\Http\Requests\EditorialUpdateRequest;

class EditorialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $editorials = Editorial::orderBy('id', 'DESC')->get();

        return view('editorials.index', compact('editorials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('editorials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EditorialStoreRequest $request)
    {
        //Validar
        $editorial = Editorial::create($request->all());

        return redirect()->route('editoriales.index')
            ->with('info', 'Editorial creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $editorial = Editorial::find($id);
        return view('editorials.show', compact('editorial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editorial = Editorial::find($id);
        return view('editorials.edit', compact('editorial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditorialUpdateRequest $request, $id)
    {
        //Validar
        $editorial = Editorial::find($id);

        $editorial->fill($request->all())->save();

        return redirect()->route('editoriales.edit', $editorial->id)
            ->with('info', 'Editorial actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $editorial = Editorial::find($id);
        $editorial->delete();

        return redirect()->route('editoriales.index')
            ->with('info', 'Editorial eliminada exitosamente.');
    }
}
