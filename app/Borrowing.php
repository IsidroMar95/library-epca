<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrowing extends Model
{
    public $timestamps = false;

    protected $fillable = ['costumer_id', 'startDate', 'finishDate', 'realDate', 'details'];

    public function costumer()
    {
        return $this->belongsTo(Costumer::class);
    }

    public function copies()
    {
        return $this->belongsToMany(Copy::class);
    }


}
