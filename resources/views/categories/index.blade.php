@extends('layout') 
@section('content')

<h2 class="mb-4 mt-4">Categorias <i class="fas fa-sad-cry"></i> </h2>

<a class="btn btn-success mt-2 mb-3" href="{{ route('categorias.create') }}" role="button">Añadir nueva categoria <i class="fas fa-plus"></i></a>



<div class="card mb-4">
    <div class="card-body">
        <table id="table-records" class="table table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th>Categoría</th>
                    <th class="actions">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->id}}</td>
                    <td>{{ $category->name}}</td>
                    <td>
                        <a href="{{ route('categorias.edit',$category->id)}}" class="btn btn-icon btn-warning" data-toggle="tooltip" title="Editar"><i class="fa fa-fw fa-edit"></i></a>
                        <form class="d-inline" action="{{ route('categorias.destroy', $category->id) }}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-fw fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@stop
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#table-records').dataTable( {
                "language": {
                "url": "{{ asset('js/Spanish.json') }}"
                 },
                 "ordering": false,
                 "pageLength": 5
            } );
        });
    </script>
@endsection