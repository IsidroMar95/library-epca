<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    $title = $faker->sentence(2);
    return [
        'title'         => $title,
        'author_id'     => rand(1, 5),
        'editorial_id'  => rand(1, 3),
        'category_id'   => rand(1, 10),
        'isbn'          => $faker->text(13),
        'slug'          => str_slug($title),
    ];
});
