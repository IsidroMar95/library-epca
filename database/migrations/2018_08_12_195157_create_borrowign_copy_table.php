<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowignCopyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowing_copy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('borrowing_id')->unsigned();
            $table->integer('copy_id')->unsigned();
            $table->string('details', 200)->nullable();
            $table->enum('returned', ['YES', 'NO'])->default('NO');

            // Relaciones
            $table->foreign('borrowing_id')->references('id')->on('borrowings')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('copy_id')->references('id')->on('copies')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowing_copy');
    }
}
