@extends('layout') 
@section('content')

<h2 class="mb-4 mt-4">Clientes <i class="fas fa-glasses"></i> </h2>

<a class="btn btn-success mt-2 mb-3" href="{{ route('clientes.create') }}" role="button">Añadir nuevo Cliente <i class="fas fa-plus"></i></a>


<div class="card mb-4">
    <div class="card-body">
        <table id="table-records" class="table table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><i class="fab fa-slack-hash"></i></th>
                    <th>Nombre</th>
                    <th>Apellido P</th>
                    <th>Apellido M</th>
                    <th>Teléfono</th>
                    <th>Email</th>
                    <th class="actions">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($costumers as $costumer)
                <tr>
                    <td>{{ $costumer->id}}</td>
                    <td>{{ $costumer->name}}</td>
                    <td>{{ $costumer->first_surname}}</td>
                    <td>{{ $costumer->second_surname}}</td>
                    <td>{{ $costumer->phone}}</td>
                    <td>{{ $costumer->email}}</td>
                    <td>
                        <a href="{{ route('clientes.edit',$costumer->id)}}" class="btn btn-icon btn-warning" data-toggle="tooltip" title="Editar"><i class="fa fa-fw fa-edit"></i></a>
                        <form class="d-inline" action="{{ route('clientes.destroy', $costumer->id) }}" method="POST">
                            @csrf @method('DELETE')
                            <button type="submit" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Eliminar"><i class="fa fa-fw fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
@stop
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#table-records').dataTable( {
                "language": {
                "url": "{{ asset('js/Spanish.json') }}"
                 },
                 "ordering": false,
                 "pageLength": 5
            } );
        });
    </script>
@endsection