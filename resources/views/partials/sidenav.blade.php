<div class="sidebar sidebar-dark bg-dark">
    <ul class="list-unstyled mt-2">
        <li {{ request()->is('/')?'class=active':'' }} ><a href="{{route ('dashboard')}}"><i class="fa fa-fw fa-tachometer-alt fa-2x mr-3"></i> Dashboard</a></li>
        <li {{ request()->is('prestamos','prestamos/crear')?'class=active':'' }}><a href="{{route ('prestamos.index')}}"><i class="fas fa-handshake fa-2x mr-3"></i> Prestamos</a></li>
        <li {{ request()->is('libros')?'class=active':'' }} ><a href="{{route ('libros.index')}}"><i class="fas fa-book fa-2x mr-3"></i> Libros</a></li>
        <li {{ request()->is('ejemplares')?'class=active':'' }} ><a href="{{route('ejemplares.index')}}"><i class="fas fa-bookmark fa-2x mr-3"></i> Ejemplares</a></li>
        <li {{ request()->is('clientes')?'class=active':'' }}><a href="{{route('clientes.index')}}"><i class="fas fa-glasses fa-2x mr-3"></i> Clientes</a></li>
        <li {{ request()->is('categorias')?'class=active':'' }} ><a href="{{route('categorias.index')}}"><i class="fas fa-sad-cry fa-2x mr-3"></i> Categorías</a></li>
        <li {{ request()->is('editoriales')?'class=active':'' }}><a href="{{route('editoriales.index')}}"><i class="fas fa-atlas fa-2x mr-3"></i> Editoriales</a></li>
        <li {{ request()->is('autores','autores/crear')?'class=active':'' }}><a href="{{route('autores.index')}}"><i class="fas fa-pen-fancy fa-2x mr-3"></i> Autores</a></li>
        <li><a href="#"><i class="fas fa-question fa-2x mr-3"></i> Acerca de </a></li>
    </ul>
</div>
