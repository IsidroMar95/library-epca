<div class="form-group">
    {{Form::label('name', '*Nombre del Autor: ', ['class' => '']) }}
    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
</div>
<div class="form-group">
        {{ Form::button('Guardar <i class="fa fa-save"></i>', ['type' => 'submit', 'class' => 'btn btn-primary'] )  }}
        <a class="btn btn-danger" href="{{ route('autores.index') }}">Volver <i class="fas fa-arrow-alt-circle-left"></i></a>
</div>