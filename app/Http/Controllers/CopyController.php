<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Copy;
use App\Book;
use App\Http\Requests\CopyStoreRequest;
use App\Http\Requests\CopyUpdateRequest;

class CopyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $copies = Copy::orderBy('id', 'DESC')->get();

        return view('copies.index', compact('copies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $books = Book::orderBy('title', 'ASC')->pluck('title', 'id');
        return view('copies.create', compact('books'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CopyStoreRequest $request)
    {
         //Validar
        $copy = Copy::create($request->all());

        return redirect()->route('ejemplares.index', $copy->id)
            ->with('info', 'Ejemplar registrado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $copy = Copy::find($id);
        return view('copies.show', compact('copy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $books = Book::orderBy('title', 'ASC')->pluck('title', 'id');
        $copy = Copy::find($id);
        return view('copies.edit', compact('copy', 'books'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CopyUpdateRequest $request, $id)
    {
        //Validar
        $copy = Copy::find($id);

        $copy->fill($request->all())->save();

        return redirect()->route('ejemplares.index', $copy->id)
            ->with('info', 'Ejemplar actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $copy = Copy::find($id);
        $msjSuccess = 'Ejemplar #' . $copy->code . ' del libro ' . $copy->book->title . ' eliminado exitosamente.';
        $msjError = 'El Ejemplar #' . $copy->code . ' del libro ' . $copy->book->title . ' se encuentra en prestamo. No puede ser eliminado';

        if ($copy->status == 'PRESTADO') {
            return redirect()->route('ejemplares.index')
                ->with('error', $msjError);
        } else {
            $copy->delete();
            return redirect()->route('ejemplares.index')
                ->with('info', $msjSuccess);
        }
    }
}
